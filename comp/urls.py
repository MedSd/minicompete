from django.urls import path
from . import views

app_name = 'comp'
urlpatterns = [
    path('list/', views.userComps,name='comp_list'),
    path('add/', views.compAdd,name='add'),
    path('<slug:slug>/', views.compSite,name='detail'),
    path('<slug:slug>/register/', views.compRegister, name='comp_register'),
    path('<slug:slug>/login/', views.compLogin, name='comp_login'),
    path('<slug:slug>/admin', views.compAdmin,name='admin'),
    path('<slug:slug>/admin/round/new', views.compCreateRound,name='round_create'),
    path('<slug:slug>/admin/round/<int:rnd>/edit', views.compEditRound,name='round_edit'),
	path('<slug:slug>/admin/round/<int:rnd>/delete', views.compDeleteRound,name='round_delete'),
    path('<slug:slug>/admin/round/<int:rnd>/form/sub/', views.roundFormSub, name='submission_form'),
    path('<slug:slug>/admin/round/<int:rnd>/form/evl/', views.roundFormEvl, name='evaluation_form'),
    path('<slug:slug>/profile/', views.participantProfile, name='participant_profile'),
    path('<slug:slug>/profile/<int:rnd>/', views.roundForm, name='round_form'),
]