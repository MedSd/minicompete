import base_db
from pymongo.write_concern import WriteConcern
from pymodm import MongoModel, EmbeddedMongoModel, fields, ReferenceField
from users.models import User
db = base_db.db
# EMBEDDED MODELS
class Field(EmbeddedMongoModel):
	slug = fields.CharField(max_length=10)
	label = fields.CharField(max_length=150)
	css_classes = fields.ListField(field=fields.CharField())
	required = fields.BooleanField(default=False)
	type = fields.CharField(max_length=10) #select/text/paragraph
	tags = fields.ListField(field=fields.CharField())

class Form(EmbeddedMongoModel):
	type = fields.CharField(max_length=10)      #sub/evl
	title = fields.CharField(max_length=70)
	settings = fields.DictField()
	fields = fields.EmbeddedDocumentListField(Field)


#Model for the Rounds of a given competition
class Round(EmbeddedMongoModel):
    number = fields.IntegerField(min_value=1, max_value=10)
    title = fields.CharField(max_length=50)
    sub_start = fields.DateTimeField()
    sub_end = fields.DateTimeField()
    evl_start = fields.DateTimeField()
    evl_end = fields.DateTimeField()
    forms =  fields.EmbeddedDocumentListField(Form)


#Main Model for any  installation whether competition or anything that untap would support in the future.
class Install(MongoModel):
    slug = fields.CharField(primary_key=True)
    title = fields.CharField(max_length=100)
    owner = fields.ReferenceField(User, on_delete=ReferenceField.CASCADE)
    admins = fields.ListField(field=fields.ReferenceField(User)) #list of user objects
    rounds =  fields.EmbeddedDocumentListField(Round)

    class Meta:
        write_concern =  WriteConcern(j=True)
        connection_alias = 'comp'

    def __str__(self):
    	return self.slug
#######################################   End Install Classes  ##########################################




##The data to be submitted inside the form. (The ACTUAL FORM)
class EntryData(EmbeddedMongoModel):
    field_slug = fields.CharField()     ##For example the field that's called --> Title
    field_value = fields.CharField()   ##For example the value of the field that's called Title


##The whole submission information
class Entry(EmbeddedMongoModel):
    #The Entry should be unique per competition and round, not competition only nor round only. i.e test competition round 1 ->> Entry 1
    #There could be test competition round 2 --> Entry 1 again (The Entry is unique per competitin AND round)
    comp_ref = fields.ReferenceField(Install, on_delete=ReferenceField.CASCADE)
    round = fields.IntegerField()   #The round holding this Entry
    status = fields.CharField(max_length=100)   #The status of the submission
    sub_title = fields.CharField(max_length=200)  #the title of the submission
    sub_timestamp = fields.DateTimeField()          #the timestamp the applicant submitted the submission
    entry_data = fields.EmbeddedDocumentListField(EntryData) #the data inside the form of the submission
    average_score = fields.FloatField()     #Average score for the entry/submission from ALL the judges.
    tags = fields.ListField(field=fields.CharField(max_length=30))  #tags for the submission


class Applicant(EmbeddedMongoModel):
    applicant_ref = fields.ReferenceField(User, on_delete=ReferenceField.CASCADE)
    entries = fields.EmbeddedDocumentListField(Entry)



class Entries(MongoModel):
    applicant = fields.EmbeddedDocumentListField(Applicant) #List of user objects (participants/applicants)

    class Meta:
        write_concern =  WriteConcern(j=True)
        connection_alias = 'comp'
###############################################   End Entries Classes  #####################################################################



