from django import forms
from comp.models import Install, Round, Form, Field
from users.models import User as UserMongo, Role
import string
import random

class UserCompForm(forms.Form):
    title  = forms.CharField(max_length=100)
    slug = forms.CharField(max_length=100)
    owner = forms.CharField(max_length=50, widget=forms.HiddenInput(), required=False)

    def save(self,commit=True):
        #Get the form information
        owner   = UserMongo.objects.get({'_id':self.cleaned_data["owner"]})
        install = Install(slug=self.cleaned_data["slug"], title=self.cleaned_data["title"], owner=owner)

        install.save()
        return install




class RoundCreateForm(forms.Form):
    title  = forms.CharField(max_length=100)
    sub_start = forms.DateTimeField(label='Submission Start', widget=forms.DateInput(attrs={'type': 'date'}))
    sub_end = forms.DateTimeField(label='Submission End', widget=forms.DateInput(attrs={'type': 'date'}))
    evl_start = forms.DateTimeField(label='Evaluation Start', widget=forms.DateInput(attrs={'type': 'date'}))
    evl_end = forms.DateTimeField(label='Evaluation End', widget=forms.DateInput(attrs={'type': 'date'}))

    def save(self,commit=True):
        #Get the form information
        install   = Install.objects.get({'_id':self.cleaned_data["slug"]})
        round = {
        'number': (len(install.rounds) + 1),
        'title': self.cleaned_data["title"],
        'sub_start': self.cleaned_data["sub_start"],
        'sub_end': self.cleaned_data["sub_end"],
        'evl_start': self.cleaned_data["evl_start"],
        'evl_end': self.cleaned_data["evl_end"]
        }

        if len(install.rounds) > 0:
        	install.rounds.append(round)
        else:
        	install.rounds = [ Round(
        		number=(len(install.rounds) + 1), 
        		title=self.cleaned_data["title"], 
        		sub_start=self.cleaned_data["sub_start"],
        		sub_end=self.cleaned_data["sub_end"],
        		evl_start=self.cleaned_data["evl_start"],
        		evl_end=self.cleaned_data["evl_end"]
        		) ]

        install.save()
        return install

    def update(self):
    	install   = Install.objects.get({'_id':self.cleaned_data["slug"]})
    	rounds = list(install.rounds)
    	for key, round in enumerate(rounds):
    		if round.number == self.cleaned_data["rnd"]:
		        install.rounds[key].title = self.cleaned_data["title"]
		        install.rounds[key].sub_start = self.cleaned_data["sub_start"]
		        install.rounds[key].sub_end = self.cleaned_data["sub_end"]
		        install.rounds[key].evl_start = self.cleaned_data["evl_start"]
		        install.rounds[key].evl_end = self.cleaned_data["evl_end"]
    	install.save()
    	return install


class FormBuilder(forms.Form):
    label = forms.CharField(max_length=150)
    required = forms.BooleanField(required=False)
    FORM_TYPES = (
        ("default", "Select Field Type"),
        ("text", "Text Field"),
        ("textarea", "Paragraph"),
        ("select", "Dropdown"),
    )
    type = forms.ChoiceField(choices = FORM_TYPES, initial='default')
    #######End Form Fields

    ##Function for generating a random string of a given length
    def randomStringGenerator(self,str_length):
        random_text =  ''.join([random.choice(string.ascii_letters + string.digits) for n in range(str_length)])
        return random_text

    def save(self, install):
        ##Getting the form entered data.
        label_text = self.cleaned_data["label"]
        required = self.cleaned_data["required"]
        type = self.cleaned_data["type"]

        ##Getting some useful information for the current round.
        round_form = self.cleaned_data["round_number"]
        round = install.rounds[round_form - 1]
        round_key = round_form - 1
        ##If there are no forms YET for this round, create a new form.
        if len(round.forms) == 0:
            form_obj =[Form(
                type=self.cleaned_data["form_type"],
                title= round.title + ' Submission Form' if self.cleaned_data["form_type"] == "sub" else round.title + ' Evaluation Form',
                fields= [Field(
                    label= label_text,
                    slug= self.randomStringGenerator(5),
                    required= required,
                    type= type
                )]
            )]
            install.rounds[round_key].forms = form_obj
            install.save()
            return install


        ##The form is there and needs to be updated.
        else:
            found = False
            forms = list(install.rounds[round_key].forms)
            for key, form in enumerate(forms):
                if form.type == self.cleaned_data["form_type"]:
                    found = True
                    field = {
                        'label': label_text,
                        'slug': self.randomStringGenerator(5),
                        'required': required,
                        'type': type
                    }
                    install.rounds[round_key].forms[key].fields.append(field)
                    break
            if not found:
                #Means the only form that exists is not the type that we want. evl not sub and vice versa
                form_obj = Form(
                    type=self.cleaned_data["form_type"],
                    title=round.title + ' Submission Form' if self.cleaned_data["form_type"] == "sub" else round.title + ' Evaluation Form',
                    fields=[Field(
                        label=label_text,
                        slug=self.randomStringGenerator(5),
                        required=required,
                        type=type)
                    ]
                )
                install.rounds[round_key].forms.append(form_obj)
            install.save()
            return install


class CompRegisterForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Username', 'required': "required", 'class': "input pass" }),label='')
    useremail = forms.EmailField(widget=forms.EmailInput(attrs={'placeholder': 'Email address', 'required': "required", 'class': "input pass" }), label='')
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Choose a Password', 'required': "required", 'class': "input pass" }), label='')
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Confirm Password', 'required': "required", 'class': "input pass" }), label='')

    def save(self,slug):
        """
        :param install: The current competition held by the sign up/ Login Page
        :return: 1  -> Passwords do not match
        :return: 2  -> User Already Exists with role participant in this competition
        :return: user  -> User added to the competition whether as a new user completely or a previous user with previous roles in another competition
        """
        #Get the form information
        new_user_email = self.cleaned_data["useremail"]
        new_username = self.cleaned_data["username"]
        new_user_password = self.cleaned_data["password"]
        new_user_password2 = self.cleaned_data["password2"]
        if new_user_password != new_user_password2:
            return 1

        try:
            user_mongo = UserMongo.objects.get({'_id': new_user_email})
            if user_mongo.roles:
                for role in user_mongo.roles:
                    if role.comp_ref == slug:
                        return 2        #The case where the user already exists in the competition
                    else:
                        user_mongo.roles.append(Role(comp_ref = slug, role = 'participant'))  #Email there and roles available but NOT in this competition

            else:
                ##The case where the email exists but no roles associated yet.
                user_mongo.roles = [ Role(comp_ref = slug, role = 'participant') ]
            user_mongo.save()
            return user_mongo

        except UserMongo.DoesNotExist:
            user = UserMongo()
            #Place the new information into the user object and save it into mongo.
            user.email = new_user_email
            user.password = new_user_password
            user.username = new_username
            user.roles = [ Role(comp_ref = slug, role = 'participant') ]
            user.save()
            return user

class CompLoginForm(forms.Form):
    useremail = forms.EmailField(widget=forms.EmailInput(attrs={'placeholder': 'Email address', 'required': "required", 'class': "input pass" }), label='')
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Enter your Password', 'required': "required", 'class': "input pass" }), label='')

    def save(self,request,slug):
        ##Get the Login Data, Email and Password
        submitted_email = self.cleaned_data["useremail"]
        submitted_password = self.cleaned_data["password"]

        try:
            user = UserMongo.objects.get({'_id': submitted_email})
            if user.password == submitted_password:
                for role in user.roles:
                    if role.comp_ref == slug:
                        request.session['_auth_user_id'] = user.email
                        # request.session['next'] = 'posts:posts_list'
                        request.user = user
                        return user
                return 1   #User doesn't exist in this competition
            else:
                return 2  # Invalid Password entered

        except UserMongo.DoesNotExist:
            return 3  # Invalid Username entered