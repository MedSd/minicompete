from django.shortcuts import render,redirect
from django.contrib import messages
from users.models import User
from .models import Install, Entry, Entries, Applicant, EntryData, db
from decorators import user_loggedin
from .forms import UserCompForm, RoundCreateForm, FormBuilder, CompRegisterForm, CompLoginForm
from datetime import datetime

@user_loggedin
def userComps(request):
	installs = Install.objects.all()
	return render(request, 'comp/list.html', {'installs': installs})

@user_loggedin
def compAdd(request):
	if request.method == 'POST':
		form = UserCompForm(request.POST)
		if form.is_valid():
			form.cleaned_data['owner'] = request.session['_auth_user_id']
			form.save()
			return redirect('comp:comp_list')
	else:
		form = UserCompForm()
	return render(request,'comp/add.html',{'form': form})


@user_loggedin
def compAdmin(request, slug):
	install = Install.objects.get({'_id': slug})
	return render(request, 'comp/admin.html', {'install': install})


@user_loggedin
def compCreateRound(request, slug):
	if request.method == 'POST':
		form = RoundCreateForm(request.POST)
		if form.is_valid():
			form.cleaned_data['slug'] = slug
			form.save()
			return redirect('comp:admin', slug=slug)
	else:
		form = RoundCreateForm()
	install = Install.objects.get({'_id': slug})
	return render(request, 'comp/new_round.html', {'install': install, 'form': form})



@user_loggedin
def compEditRound(request, slug, rnd):
	rund = None
	install = Install.objects.get({'_id': slug})
	for round in install.rounds:
		if round.number == rnd:
			rund = round
	if request.method == 'POST':
		form = RoundCreateForm(request.POST)
		if form.is_valid():
			form.cleaned_data['slug'] = slug
			form.cleaned_data['rnd'] = rnd
			form.update()
			return redirect('comp:admin', slug=slug)
	else:
		form = RoundCreateForm(initial={'title': rund.title, 'sub_start': rund.sub_start, 'sub_end': rund.sub_end, 'evl_start': rund.evl_start, 'evl_end': rund.evl_end})
	return render(request, 'comp/edit_round.html', {'install': install, 'form': form, 'rnd': rnd})


@user_loggedin
def compDeleteRound(request, slug, rnd):
	install = Install.objects.get({'_id': slug})
	del install.rounds[rnd - 1]
	install.save()
	return redirect('comp:admin', slug=slug)

@user_loggedin
def roundFormSub(request,slug, rnd):
	install = Install.objects.get({'_id': slug})
	round = install.rounds[rnd - 1]
	if request.method == 'POST':
		form = FormBuilder(request.POST)
		if form.is_valid():
			form.cleaned_data['form_type'] = "sub"
			form.cleaned_data['round_number'] = rnd
			form.save(install)
			return redirect('comp:submission_form', slug=slug, rnd=rnd)
	else:
		form = FormBuilder()
	return render(request,'comp/sub_form.html', {'install': install, 'round': round , 'form': form})


@user_loggedin
def roundFormEvl(request,slug, rnd):
	install = Install.objects.get({'_id': slug})
	round = install.rounds[rnd -1]
	if request.method == 'POST':
		form = FormBuilder(request.POST)
		if form.is_valid():
			form.cleaned_data['form_type'] = "evl"
			form.cleaned_data['round_number'] = rnd
			form.save(install)
			return redirect('comp:evaluation_form', slug=slug, rnd=rnd)
	else:
		form = FormBuilder()
	return render(request,'comp/evl_form.html', {'install': install, 'round': round , 'form': form})



def compSite(request, slug):
	install = Install.objects.get({'_id': slug})
	register_form = CompRegisterForm()
	login_form = CompLoginForm()
	return render(request, 'comp/detail.html', {'install': install, 'register_form': register_form, 'login_form': login_form})



def compRegister(request,slug):
	"""
	A function that registers a user to the competition
	"""
	if request.method == 'POST':
		form = CompRegisterForm(request.POST)
		if form.is_valid():
			user = form.save(slug)
			if user == 1:
				messages.add_message(request,messages.INFO, 'Passwords do not match')

			elif user == 2:
				messages.add_message(request,messages.INFO, 'The user already exists in this competition. Please Login')
			else:
				messages.add_message(request,messages.INFO, 'User is added successfully to the competition')

			return redirect('comp:detail', slug=slug)



def compLogin(request,slug):
	if request.method == 'POST':
		form = CompLoginForm(request.POST)
		if form.is_valid():
			user = form.save(request,slug)
			if user == 1:
				messages.add_message(request,messages.WARNING, 'User Does not exist in this competition')
			elif user == 2:
				messages.add_message(request,messages.WARNING, 'Invalid Password Entered. Please try again.')
			elif user == 3:
				messages.add_message(request,messages.WARNING, 'Invalid Username entered. Please try again')
			else:
				messages.add_message(request, messages.WARNING, 'User is logged in successfully!')
				return redirect('comp:participant_profile', slug=slug)

			return redirect('comp:detail', slug=slug)


def participantProfile(request,slug):
	install = Install.objects.get({'_id': slug})
	return render(request,'comp/participant_profile.html', {'install': install})

def roundForm(request,slug,rnd):
	install = Install.objects.get({'_id':slug})
	round_forms = install.rounds[rnd - 1].forms
	if request.method == 'POST':
		user = User.get_current_user(request)
		entry_key = None
		entry_data = []
		for key, data in request.POST.items():
			if key == 'csrfmiddlewaretoken':
				continue
			entry_data.append(EntryData(field_slug=key, field_value=data))

		if Entries.objects.count() > 0:
			ents = list(Entries.objects.all())
			app_key = None
			user_exists, entry_exists, same_round = (False,False,False)
			for et_key, entry in enumerate(ents):
				applicant_list = list(entry.applicant)
				for key, app in enumerate(applicant_list):
					if app.applicant_ref == user:
						user_exists = True
						app_key = key
						entry_key = et_key
						applicant_entries = list(app.entries)
						for ent_key, ent in enumerate(applicant_entries):
							if ent.comp_ref.slug == slug:
								entry_exists = True
								if ent.round == rnd:
									same_round = True
									break
						break
		else:
			app_entry = [Entry(comp_ref=slug, round=rnd, status='applied', sub_timestamp=datetime.now(), entry_data=entry_data)]
			applicant = [Applicant(applicant_ref=user, entries=app_entry)]
			entry = Entries(applicant=applicant)
			entry.save()
			return redirect('comp:participant_profile', slug=slug)

		####Checkings for adding a new entry to the database.
		if not user_exists:
			##User is not found in the entries collection, so add a completely new applicant to the list.
			app_entry = [Entry(comp_ref=slug, round=rnd,status='applied', sub_timestamp=datetime.now(),entry_data=entry_data)]
			applicant = [Applicant(applicant_ref=user, entries=app_entry)]
			entry = Entries(applicant= applicant)
			entry.save()
			return redirect('comp:participant_profile', slug=slug)
		elif (user_exists and not entry_exists) or (user_exists and entry_exists and not same_round):
			app_entry = Entry(comp_ref=slug, round=rnd, status='applied', sub_timestamp=datetime.now(), entry_data=entry_data)
			entrs = Entries.objects.all()
			enry = entrs[entry_key]
			enry.applicant[app_key].entries.append(app_entry)
			enry.save()
			return redirect('comp:participant_profile', slug=slug)

		elif user_exists and entry_exists and same_round:
			messages.add_message(request, messages.INFO, 'You already submitted this application before.')
			##ERROR, EXISTING ENTRY ALREADY THERE.

	final_form = None
	for form in round_forms:
		if form.type == "sub":
			final_form = form

	return render(request,'comp/round_form.html', {'form': final_form, 'round': rnd, 'slug': slug})
