import base_db
from pymongo.write_concern import WriteConcern
from pymodm import MongoModel, EmbeddedMongoModel, fields, ReferenceField

# EMBEDDED MODELS
class Name(EmbeddedMongoModel):
    first = fields.CharField(max_length=50)
    last = fields.CharField(max_length=50)


class Role(EmbeddedMongoModel):
    comp_ref = fields.CharField(max_length=20)
    role = fields.CharField(max_length=20)


#Main Model
class User(MongoModel):
    email = fields.EmailField(primary_key=True)
    username = fields.CharField(max_length=50)
    password = fields.CharField(max_length=50)
    name = fields.EmbeddedDocumentField(Name)
    roles =  fields.EmbeddedDocumentListField(Role)
    last_access = fields.DateTimeField()
    created_at = fields.DateTimeField()
    extra = fields.DictField()

    class Meta:
        write_concern =  WriteConcern(j=True)
        connection_alias = 'comp'

    def __str__(self):
    	return self.email

    def get_current_user(request):
    	pk = request.session['_auth_user_id']
    	return User.objects.get({'_id': pk}) if '_auth_user_id' in request.session else None

    def is_user_loggedin(request):
    	if '_auth_user_id' in request.session:
    		pk = request.session['_auth_user_id']  
    		return True if User.objects.get({'_id': pk}) is not None else False
    	else: 
    		return False


    def is_admin(request):
        if User.is_user_loggedin(request):
            pk = request.session['_auth_user_id']
            user = User.objects.get({'_id': pk})
            for role in user.roles:
                if role.comp_ref == 'main' and role.role == 'admin':
                    if '_authorized' not in request.session:
                        request.session['_authorized'] = 1
                    return True
        return False


