from django.shortcuts import render,redirect
from users.forms import UserCreationForm, UserLoginForm, UserProfileForm
from .models import User, Role
from decorators import user_loggedin
import datetime
from django.http import JsonResponse

# Create your views here.
def loginview(request):
    message = None
    if request.method == 'POST':
        form = UserLoginForm(request.POST)
        if form.is_valid():
            user = form.login(request=request)
            if user == 1:
                message = 'Invalid Password Entered. Please try again'
            elif user == 2:
                message = 'There is no username with this email.'
            else:
                User.is_admin(request)
                user.last_access = datetime.datetime.now()
                user.save()
                #User is authenticated successfully, redirect to the profile page.
                return redirect('users:profile')

    else:
        form = UserLoginForm()

    if '_auth_user_id' not in request.session:
        return render(request,'users/login.html',{'form': form, 'error_msg': message})
    else:
        return redirect('users:profile')



def registerview(request):
    message = None
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            message = form.save()
            return redirect('users:profile')
    else:
        form = UserCreationForm()
    return render(request,'users/sign_up.html',{'form': form, 'error_msg': message})



def logoutview(request):
	request.session.flush()
	return redirect('users:login_user')


@user_loggedin
def profileview(request):
    if request.method == 'POST':
        form = UserProfileForm(request.POST)
        if form.is_valid():
            form.cleaned_data['email'] = request.session['_auth_user_id']
            user = form.save()
            return redirect('users:profile')
    else:
        user = User.get_current_user(request)
        if user.extra:
            form = UserProfileForm(initial={'interests': user.extra['interests'],'job': user.extra['job'],'edu_level': user.extra['edu_level'], 'first': user.name.first, 'last': user.name.last})
        else:
            form = UserProfileForm()
    return render(request,'users/profile.html',{'form': form, 'user': user})


@user_loggedin
def usersview(request):
    if User.is_admin(request):
        users = User.objects.all()
        return render(request, 'users/all.html', {'users': users})
    else:
        return redirect('users:profile')



@user_loggedin
def deleteuser(request, username):
    if User.is_admin(request):
        user = User.objects.get({'username': username})
        user.delete()
    return redirect('users:all')


@user_loggedin
def edituser(request, username):
    if User.is_admin(request) and request.method == 'POST':
        user = User.objects.get({'username': username})
        user.roles = [ Role(comp_ref = 'main', role = request.POST['role']) ]
        user.save()
    return redirect('users:all')