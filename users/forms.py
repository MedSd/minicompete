from django.contrib.auth.forms import UserCreationForm
from django import forms
from users.models import User as UserMongo
from users.models import Name
import logging


class UserCreationForm(UserCreationForm):
    email = forms.EmailField(label="Email", required=True)


    def save(self,commit=True):
        if commit:
            user = UserMongo()
            #Get the form information
            new_user_email = self.cleaned_data["email"]
            new_user_password = self.cleaned_data["password1"]
            new_user_username = self.cleaned_data["username"]

            try:
                UserMongo.objects.get({'_id': new_user_email})
                return 'User Already exists with this email'
            except UserMongo.DoesNotExist:
                #Place the new information into the user object and save it into mongo.
                user.email = new_user_email
                user.password = new_user_password
                user.username = new_user_username
                user.save()
                return 'User is created successfully!'

class UserLoginForm(forms.Form):
    email = forms.EmailField(label="Email", required=True)
    password = forms.CharField(label="Password", widget=forms.PasswordInput)

    def login(self,request,commit=True):
        """
          A function that performs the save function of the login view.
          THE POST function of the Login form.
        """
        if commit:
           ##Get the Login Data, Email and Password
            submitted_email = self.cleaned_data["email"]
            submitted_password = self.cleaned_data["password"]

            try:
                user = UserMongo.objects.get({'_id': submitted_email})
                if user.password == submitted_password:
                    request.session['_auth_user_id'] = user.email
                    # request.session['next'] = 'posts:posts_list'
                    request.user = user
                    return user
                else:
                    return 1  # Invalid Password entered

            except UserMongo.DoesNotExist:
                return 2  # Invalid Username entered
        


    
class UserProfileForm(forms.Form):
    first = forms.CharField(label="First Name")
    last = forms.CharField(label="Last Name")
    interests = forms.CharField(label="Interests")
    job = forms.CharField(label="Job")
    edu_level = forms.CharField(label="Education Level")

    def save(self,commit=True):
        #Get the form information
        extras = {
        'interests': self.cleaned_data["interests"],
        'job': self.cleaned_data["job"],
        'edu_level': self.cleaned_data["edu_level"]
        }

        user = UserMongo.objects.get({'_id': self.cleaned_data["email"]})
        user.extra = extras
        user.name = {'first': self.cleaned_data["first"], 'last': self.cleaned_data["last"]}
        user.save()
        return user



