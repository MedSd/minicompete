from django.urls import path
from . import views

app_name = 'users'
urlpatterns = [
    path('login/', views.loginview,name='login_user'),
    path('register/', views.registerview,name='register_user'),
    path('logout/', views.logoutview,name='logout_user'),
    path('profile/', views.profileview,name='profile'),
    path('all/', views.usersview,name='all'),
    path('<slug:username>/delete/', views.deleteuser,name='delete'),
    path('<slug:username>/edit/', views.edituser,name='edit'),
]