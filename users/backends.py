from users.models import User
import logging

class MyAuthBackend(object):
    def authenticate(self,email,password):
        try:
            user = User.collection.find({'email': email})
            if user.password == password:
                return user
            else:
                return None
        except User.DoesNotExist:
            logging.getLogger("error_logger").error("User does not exist")
            return None
        except Exception as e:
            logging.getLogger("error_logger").error(repr(e))
            return None

    def get_user(self,user_id):
        try:
            user = User.collection.find({"_id": user_id})
            return user

        except User.DoesNotExist:
            logging.getLogger("error_logger").error("user with {} not found".format(user_id))
            return None



    





