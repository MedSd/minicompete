from users.models import User
from django.shortcuts import redirect

def user_loggedin(check_func):
	def wrapper(request, *args, **kwargs):
		if User.is_user_loggedin(request=request):
			return check_func(request, *args, **kwargs)
		else:
			return redirect('users:login_user')
	return wrapper